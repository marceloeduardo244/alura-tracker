import http from "@/http";
import ITarefa from "@/interfaces/ITarefa";
import { Module } from "vuex";
import { State } from "..";
import {
  OBTER_TAREFAS,
  CADASTRAR_TAREFAS,
  ALTERAR_TAREFAS,
} from "../tipo-acoes";
import {
  DEFINIR_TAREFAS,
  ADICIONA_TAREFA,
  ALTERA_TAREFA,
} from "../tipo-mutacoes";

export interface EstadoTarefa {
  tarefas: ITarefa[];
}

export const tarefa: Module<EstadoTarefa, State> = {
  mutations: {
    [DEFINIR_TAREFAS](state, tarefas: ITarefa[]) {
      state.tarefas = tarefas;
    },
    [ADICIONA_TAREFA](state, tarefa: ITarefa) {
      state.tarefas.push(tarefa);
    },
    [ALTERA_TAREFA](state, tarefa: ITarefa) {
      const index = state.tarefas.findIndex((taref) => taref.id == tarefa.id);
      if (index !== undefined || index !== null) {
        state.tarefas[index] = tarefa;
      }
    },
  },
  actions: {
    [OBTER_TAREFAS]({ commit }, filtro: string) {
      let url = "/tarefas";

      if (filtro) {
        url += "?descricao=" + filtro;
      }
      http.get(url).then((response) => commit(DEFINIR_TAREFAS, response.data));
    },
    [CADASTRAR_TAREFAS]({ commit }, tarefa: ITarefa) {
      return http.post("/tarefas", tarefa).then((response) => {
        commit(ADICIONA_TAREFA, response.data);
      });
    },
    [ALTERAR_TAREFAS]({ commit }, tarefa: ITarefa) {
      return http.put(`/tarefas/${tarefa.id}`, tarefa).then(() => {
        commit(ALTERA_TAREFA, tarefa);
      });
    },
  },
};
