import http from "@/http";
import IProjeto from "@/interfaces/IProjeto";
import { Module } from "vuex";
import { State } from "..";
import {
  OBTER_PROJETOS,
  CADASTRAR_PROJETOS,
  ALTERAR_PROJETOS,
  REMOVER_PROJETOS,
} from "../tipo-acoes";
import {
  ADICIONA_PROJETO,
  ALTERA_PROJETO,
  EXCLUI_PROJETO,
  DEFINIR_PROJETOS,
} from "../tipo-mutacoes";

export interface EstadoProjeto {
  projetos: IProjeto[];
}

export const projeto: Module<EstadoProjeto, State> = {
  mutations: {
    [ADICIONA_PROJETO](state, nomeDoProjeto: string) {
      const projeto = {
        id: new Date().toISOString(),
        nome: nomeDoProjeto,
      } as IProjeto;
      state.projetos.push(projeto);
    },
    [ALTERA_PROJETO](state, projeto: IProjeto) {
      const index = state.projetos.findIndex((proj) => proj.id == projeto.id);
      if (index !== undefined || index !== null) {
        state.projetos[index] = projeto;
      }
    },
    [EXCLUI_PROJETO](state, id: string) {
      state.projetos = state.projetos.filter((proj) => proj.id != id);
    },
    [DEFINIR_PROJETOS](state, projetos: IProjeto[]) {
      state.projetos = projetos;
    },
  },
  actions: {
    [OBTER_PROJETOS]({ commit }) {
      http
        .get("/projetos")
        .then((response) => commit(DEFINIR_PROJETOS, response.data));
    },
    [CADASTRAR_PROJETOS](state, nomeDoProjeto: string) {
      return http.post("/projetos", {
        nome: nomeDoProjeto,
      });
    },
    [ALTERAR_PROJETOS](state, projeto: IProjeto) {
      return http.put(`/projetos/${projeto.id}`, projeto);
    },
    [REMOVER_PROJETOS](state, idProjeto: string) {
      return http.delete(`/projetos/${idProjeto}`).then(() => {
        state.commit(EXCLUI_PROJETO, idProjeto);
      });
    },
  },
};
