import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";
import ViewTarefas from "../views/Tarefas.vue";
import ViewProjetos from "../views/Projetos.vue";
import ViewFormularioProjeto from "../views/Projetos/Formulario.vue";
import ViewListaDeProjetos from "../views/Projetos/Lista.vue";

const routes: RouteRecordRaw[] = [
  {
    path: "/",
    name: "Tarefas",
    component: ViewTarefas,
  },
  {
    path: "/projetos",
    component: ViewProjetos,
    children: [
      {
        path: "",
        name: "ViewListaDeProjetos",
        component: ViewListaDeProjetos,
      },
      {
        path: "novo",
        name: "Novo projeto",
        component: ViewFormularioProjeto,
      },
      {
        path: ":id",
        name: "Editar projeto",
        component: ViewFormularioProjeto,
        props: true,
      },
    ],
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
