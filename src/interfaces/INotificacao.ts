export enum TipoNOtificacao {
  SUCESSO,
  FALHA,
  ATENCAO,
}

export interface INotificacao {
  titulo: string;
  texto: string;
  tipo: TipoNOtificacao;
  id: number;
}
