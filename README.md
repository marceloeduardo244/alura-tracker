# alura-tracker

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Install and run the local database

```
yarn add -g json-server
json-server --watch  banco-de-dados.json
```

### Compiles and minifies for production

```
yarn build
```

### Lints and fixes files

```
yarn lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
